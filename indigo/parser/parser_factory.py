from indigo.parser.parser import Parser
from indigo.parser.rules.constant_rule import parse_constant
from indigo.parser.rules.methodcall_rule import parse_methodcall
from indigo.parser.rules.symbol_rule import parse_symbol
from indigo.parser.rules.expression_rule import parse_expression
from indigo.parser.parser_token_types import ParserTokenTypes

def build(lexer):
    parser = Parser(lexer, {
        ParserTokenTypes.CONSTANT: parse_constant,
        ParserTokenTypes.SYMBOL: parse_symbol,
        ParserTokenTypes.EXPRESSION: parse_expression,
        ParserTokenTypes.METHODCALL: parse_methodcall
    })
    return parser
