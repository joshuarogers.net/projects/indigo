class MethodCallParserToken:
    def __init__(self, method, arguments):
        self.method = method
        self.arguments = arguments
    
    def __eq__(self, other):
        return type(self) == type(other) and self.method == other.method and self.arguments == other.arguments

    def __ne__(self, other):
        return not self.__eq__(other)

