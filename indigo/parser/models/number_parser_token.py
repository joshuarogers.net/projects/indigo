from enum import Enum

class NumberTypes(Enum):
    INTEGER = 1
    DECIMAL = 2
    
    
class NumberParserToken:
    def __init__(self, value, number_type):
        self.value = value
        self.number_type = number_type
    
    def __eq__(self, other):
        return type(self) == type(other) and self.value == other.value and self.number_type == other.number_type
        
    def __ne__(self, other):
        return not self.__eq__(other)

