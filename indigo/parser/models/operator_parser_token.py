from enum import Enum

class OperatorTypes(Enum):
    ADDITION = 1,
    SUBTRACTION = 2,
    MULTIPLICATION = 3,
    DIVISION = 4,
    MODULUS = 5,
    EXPONENT = 6

class OperatorParserToken:
    def __init__(self, operator, expr1, expr2):
        self.operator = operator
        self.expr1 = expr1
        self.expr2 = expr2
        
    def __eq__(self, other):
        return type(self) == type(other) and self.operator == other.operator and self.expr1 == other.expr1 and self.expr2 == other.expr2
        
    def __ne__(self, other):
        return not self.__eq__(other)

