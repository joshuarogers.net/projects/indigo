class SymbolParserToken:
    def __init__(self, value):
        self.value = value
        
    def __eq__(self, other):
        return type(self) == type(other) and self.value == other.value
        
    def __ne__(self, other):
        return not self.__eq__(other)

