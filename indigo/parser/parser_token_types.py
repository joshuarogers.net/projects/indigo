from enum import Enum

class ParserTokenTypes(Enum):
    CONSTANT = 1
    SYMBOL = 2
    EXPRESSION = 3
    METHODCALL = 4
