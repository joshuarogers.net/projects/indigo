from enum import Enum
from indigo.lexer.lexer import LexerTokenTypes
from .parser_token_types import ParserTokenTypes


class ParserException(Exception):
    pass


class Parser:
    def __init__(self, seekable_lexer, parser_rules):
        self._seekable_lexer = seekable_lexer
        self._parser_rules = parser_rules
                        
    def _lex(self, token_type, testfn = None, ignore_whitespace = True):
        position = self._seekable_lexer.position()
        
        token = self._seekable_lexer.get_next_token()
        while ignore_whitespace == True and token is not None and token.type == LexerTokenTypes.WHITESPACE:
            token = self._seekable_lexer.get_next_token()
        
        if token is None:
            return None
            
        if token.type == token_type and (testfn == None or testfn(token)):
            return token
            
        self._seekable_lexer.seek(position)
        return None
    
    def _parse(self, parser_token_type):
        if parser_token_type in self._parser_rules:
            position = self._seekable_lexer.position()

            parser_token = self._parser_rules[parser_token_type](self._lex, self._parse)
            if parser_token is not None:
                return parser_token

            self._seekable_lexer.seek(position)
        return None
                        
    def parse(self, parser_token_type):
        parser_token = self._parse(parser_token_type)
        if parser_token is not None:
            return parser_token
        raise(ParserException("Failed to parse"))

