from indigo.lexer import LexerTokenTypes
from indigo.parser.models import *
from indigo.parser.parser import ParserException
from indigo.parser.parser_token_types import ParserTokenTypes


_operator_lexer_tokens = {
    LexerTokenTypes.PLUS: OperatorTypes.ADDITION,
    LexerTokenTypes.MINUS: OperatorTypes.SUBTRACTION,
    LexerTokenTypes.ASTRISK: OperatorTypes.MULTIPLICATION,
    LexerTokenTypes.FORWARDSLASH: OperatorTypes.DIVISION,
    LexerTokenTypes.MODULUS: OperatorTypes.MODULUS,
    LexerTokenTypes.CARET: OperatorTypes.EXPONENT
}

_operator_split_priority = {
    OperatorTypes.EXPONENT: 3,
    OperatorTypes.MULTIPLICATION: 2,
    OperatorTypes.DIVISION: 2,
    OperatorTypes.MODULUS: 2,
    OperatorTypes.ADDITION: 1,
    OperatorTypes.SUBTRACTION: 1
}

_operator_split_max_priority = max([_operator_split_priority[x] for x in _operator_split_priority])

def _parse_operator(lex, parse):
    for operator_lexer_token in _operator_lexer_tokens:
        if lex(operator_lexer_token) is not None:
            return _operator_lexer_tokens[operator_lexer_token]
    return None

def _build_tree(tokens, operators):
    if len(operators) == 1:
        return OperatorParserToken(operators[0], tokens[0], tokens[1])
    
    if len(tokens) == 1:
        return tokens[0]
    
    # Scan all of the operators to find the first operator that has the lowest remaining priority
    for current_operator_priority in range(_operator_split_max_priority):
        for operator_index in range(len(operators)):
            operator = operators[operator_index]
            
            if _operator_split_priority[operator] != current_operator_priority:
                continue
            
            return OperatorParserToken(operator,
                _build_tree(tokens[:operator_index + 1], operators[:operator_index]),
                _build_tree(tokens[operator_index + 1:], operators[operator_index + 1:]))

def parse_expression(lex, parse):
    tokens = []
    operators = []
    
    while True:
        token = parse(ParserTokenTypes.CONSTANT) or parse(ParserTokenTypes.METHODCALL) or parse(ParserTokenTypes.SYMBOL)
        if token is None:
            break
        tokens.append(token)
    
        operator = _parse_operator(lex, parse)
        if operator is None:
            break
        operators.append(operator)

    if len(tokens) == len(operators):
        raise ParserException("An expression cannot end with an operator")

    return ExpressionParserToken(_build_tree(tokens, operators))

