from enum import Enum
from indigo.lexer.lexer import LexerTokenTypes
from indigo.parser.models import *
        
        
def value_or(token, default):
    return token.value if token is not None else default
    

def _parse_number(lex, parse):
    sign = "-" if lex(LexerTokenTypes.MINUS) else ""
    integer_token = lex(LexerTokenTypes.NUMBER)
    has_decimal = lex(LexerTokenTypes.PERIOD)
    if integer_token is None and has_decimal is None:
        return None
        
    integer = value_or(integer_token, 0)
    if has_decimal is None:
        return NumberParserToken(int(float("{}{}".format(sign, integer))), NumberTypes.INTEGER)
    
    decimal = value_or(lex(LexerTokenTypes.NUMBER), 0)
    return NumberParserToken(float("{}{}.{}".format(sign, integer, decimal)), NumberTypes.DECIMAL)
    
def _parse_string(lex, parse):
    string = lex(LexerTokenTypes.STRING)
    return StringParserToken(string.value) if string is not None else None

def parse_constant(lex, parse):
    return _parse_string(lex, parse) or _parse_number(lex, parse)
