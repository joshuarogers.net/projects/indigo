from indigo.lexer.lexer import LexerTokenTypes
from indigo.parser.parser_token_types import ParserTokenTypes
from indigo.parser.models import *
        
    
_reserved_words = set([
    "let"
])


def parse_symbol(lex, parse):
    symbol = lex(LexerTokenTypes.WORD)
    if symbol is None:
        return None
    if symbol.value.lower() in _reserved_words:
        return None

    return SymbolParserToken(symbol.value)
