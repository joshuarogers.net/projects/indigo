def build_fake_lex(tokens):
    def read(token_type):
        if len(tokens) == 0:
            return None
        if tokens[0].type != token_type:
            return None
        return tokens.pop(0)
    return read
    
def build_fake_parse(tokens):
    def read(token_type):
        if len(tokens) == 0:
            return None
        if tokens[0]["type"] != token_type:
            return None
        return tokens.pop(0)["value"]
    return read
