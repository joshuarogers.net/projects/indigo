import pytest

from fakes import build_fake_lex
from indigo.lexer.lexer import LexerToken, LexerTokenTypes
from indigo.parser.models import *
from indigo.parser.rules.constant_rule import parse_constant

    
def test_parse_positive_integer():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.NUMBER, "1")
    ])

    token = parse_constant(lex, None)
    assert type(token) is NumberParserToken
    assert token.value == 1
    assert NumberTypes.INTEGER == token.number_type

def test_parse_negative_integer():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.MINUS),
        LexerToken(LexerTokenTypes.NUMBER, "1")
    ])

    token = parse_constant(lex, None)
    assert type(token) is NumberParserToken
    assert -1 == token.value
    assert NumberTypes.INTEGER == token.number_type

def test_parse_zero():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.NUMBER, "0")
    ])
    
    token = parse_constant(lex, None)
    assert type(token) is NumberParserToken
    assert 0 == token.value
    assert NumberTypes.INTEGER == token.number_type

def test_parse_large_int():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.NUMBER, "1234567890")
    ])

    token = parse_constant(lex, None)
    assert type(token) is NumberParserToken
    assert 1234567890 == token.value
    assert NumberTypes.INTEGER == token.number_type

def test_parse_decimal():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.NUMBER, "0"),
        LexerToken(LexerTokenTypes.PERIOD),
        LexerToken(LexerTokenTypes.NUMBER, "125")    
    ])
    
    token = parse_constant(lex, None)
    assert type(token) is NumberParserToken
    assert 0.125 == token.value
    assert NumberTypes.DECIMAL == token.number_type

def test_parse_negative_decimal():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.MINUS),
        LexerToken(LexerTokenTypes.NUMBER, "0"),
        LexerToken(LexerTokenTypes.PERIOD),
        LexerToken(LexerTokenTypes.NUMBER, "125")
    ])
    
    token = parse_constant(lex, None)
    assert type(token) is NumberParserToken
    assert -0.125 == token.value
    assert NumberTypes.DECIMAL == token.number_type
    
def test_parse_decimal_with_int_only():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.NUMBER, "1"),
        LexerToken(LexerTokenTypes.PERIOD)
    ])
    
    token = parse_constant(lex, None)
    assert type(token) is NumberParserToken
    assert 1 == token.value
    assert NumberTypes.DECIMAL == token.number_type
    
def test_parse_int_and_decimal():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.NUMBER, "123"),
        LexerToken(LexerTokenTypes.PERIOD),
        LexerToken(LexerTokenTypes.NUMBER, "125")
    ])
    
    token = parse_constant(lex, None)
    assert type(token) is NumberParserToken
    assert 123.125 == token.value
    assert NumberTypes.DECIMAL == token.number_type
    
def test_parse_no_leading_int():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.PERIOD),
        LexerToken(LexerTokenTypes.NUMBER, "125")
    ])
    
    token = parse_constant(lex, None)
    assert type(token) is NumberParserToken
    assert 0.125 == token.value
    assert NumberTypes.DECIMAL == token.number_type

def test_parse_negative_decimal_with_int_only():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.MINUS),
        LexerToken(LexerTokenTypes.NUMBER, "1"),
        LexerToken(LexerTokenTypes.PERIOD)
    ])
    
    token = parse_constant(lex, None)
    assert type(token) is NumberParserToken
    assert -1 == token.value
    assert NumberTypes.DECIMAL == token.number_type
    
def test_parse_negative_int_and_decimal():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.MINUS),
        LexerToken(LexerTokenTypes.NUMBER, "123"),
        LexerToken(LexerTokenTypes.PERIOD),
        LexerToken(LexerTokenTypes.NUMBER, "125")
    ])
    
    token = parse_constant(lex, None)
    assert type(token) is NumberParserToken
    assert -123.125 == token.value
    assert NumberTypes.DECIMAL == token.number_type
    
def test_parse_negative_no_leading_int():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.MINUS),
        LexerToken(LexerTokenTypes.PERIOD),
        LexerToken(LexerTokenTypes.NUMBER, "125")
    ])
    token = parse_constant(lex, None)
    assert type(token) is NumberParserToken
    assert -0.125 == token.value
    assert NumberTypes.DECIMAL == token.number_type
            
def test_parse_string():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.STRING, "Hello World")
    ])
    
    token = parse_constant(lex, None)
    assert type(token) is StringParserToken
    assert "Hello World" == token.value
    
def test_parse_returns_none_if_invalid():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.WORD, "x")
    ])
    
    token = parse_constant(lex, None)
    assert token is None

