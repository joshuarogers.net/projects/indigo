import pytest

from fakes import build_fake_lex, build_fake_parse
from indigo.lexer import LexerToken, LexerTokenTypes
from indigo.parser.parser import ParserException
from indigo.parser.parser_token_types import ParserTokenTypes
from indigo.parser.models import *
from indigo.parser.rules.expression_rule import parse_expression

def test_parse_constant_numeric():
    lex = build_fake_lex([])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(5, NumberTypes.INTEGER) }
    ])
    
    token = parse_expression(lex, parse)
    assert type(token) is ExpressionParserToken
    assert type(token.value) is NumberParserToken
    assert 5 == token.value.value

def test_parse_constant_string():
    lex = build_fake_lex([])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.CONSTANT, "value": StringParserToken("hello") }
    ])
    
    token = parse_expression(lex, parse)
    assert type(token) is ExpressionParserToken
    assert type(token.value) is StringParserToken
    assert "hello" == token.value.value
    
def test_parse_symbol():
    lex = build_fake_lex([])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.SYMBOL, "value": SymbolParserToken("username") }
    ])
    
    token = parse_expression(lex, parse)
    assert type(token) is ExpressionParserToken
    assert type(token.value) is SymbolParserToken
    assert "username" == token.value.value

def test_parse_methodcall():
    lex = build_fake_lex([])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.METHODCALL, "value": MethodCallParserToken("get_user", []) }
    ])
    
    token = parse_expression(lex, parse)
    assert type(token) is ExpressionParserToken
    assert token.value == MethodCallParserToken("get_user", [])

@pytest.mark.parametrize("lexer_tokens,operator_type", [
    ([LexerToken(LexerTokenTypes.PLUS)], OperatorTypes.ADDITION),
    ([LexerToken(LexerTokenTypes.MINUS)], OperatorTypes.SUBTRACTION),
    ([LexerToken(LexerTokenTypes.ASTRISK)], OperatorTypes.MULTIPLICATION),
    ([LexerToken(LexerTokenTypes.FORWARDSLASH)], OperatorTypes.DIVISION),
    ([LexerToken(LexerTokenTypes.CARET)], OperatorTypes.EXPONENT),
    ([LexerToken(LexerTokenTypes.MODULUS)], OperatorTypes.MODULUS)
])
def test_parse_operator_types(lexer_tokens, operator_type):
    lex = build_fake_lex(lexer_tokens)
    parse = build_fake_parse([
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(1, NumberTypes.INTEGER) },
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(2, NumberTypes.INTEGER) }
    ])
    
    token = parse_expression(lex, parse)
    
    assert token == ExpressionParserToken(OperatorParserToken(operator_type,
        NumberParserToken(1, NumberTypes.INTEGER),
        NumberParserToken(2, NumberTypes.INTEGER)))
        
def test_parse_invalid_operator_raises():
    lex = build_fake_lex([LexerToken(LexerTokenTypes.PLUS)])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(1, NumberTypes.INTEGER) }
    ])

    with pytest.raises(ParserException):
        token = parse_expression(lex, parse)

# Source: "1 ^ 2 * 3 + 4"
# Tree:
#       +
#      / \
#     *  4
#    / \
#   ^  3
#  / \
# 1  2
def test_parse_operator_precedence_descending():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.CARET),
        LexerToken(LexerTokenTypes.ASTRISK),
        LexerToken(LexerTokenTypes.PLUS)
    ])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(1, NumberTypes.INTEGER) },
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(2, NumberTypes.INTEGER) },
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(3, NumberTypes.INTEGER) },
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(4, NumberTypes.INTEGER) }
    ])
    
    token = parse_expression(lex, parse)
    
    assert token == ExpressionParserToken(OperatorParserToken(OperatorTypes.ADDITION,
        OperatorParserToken(OperatorTypes.MULTIPLICATION,
            OperatorParserToken(OperatorTypes.EXPONENT,
                NumberParserToken(1, NumberTypes.INTEGER),
                NumberParserToken(2, NumberTypes.INTEGER)),
            NumberParserToken(3, NumberTypes.INTEGER)),
        NumberParserToken(4, NumberTypes.INTEGER)))
        
# Source: "1 + 2 * 3 ^ 4"
# Tree:
#   +
#  / \
# 1  *
#   / \
#  2  ^
#    / \
#   3  4
def test_parse_operator_precedence_ascending():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.PLUS),
        LexerToken(LexerTokenTypes.ASTRISK),
        LexerToken(LexerTokenTypes.CARET)
    ])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(1, NumberTypes.INTEGER) },
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(2, NumberTypes.INTEGER) },
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(3, NumberTypes.INTEGER) },
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(4, NumberTypes.INTEGER) }
    ])
    
    token = parse_expression(lex, parse)
    
    assert token == ExpressionParserToken(OperatorParserToken(OperatorTypes.ADDITION,
        NumberParserToken(1, NumberTypes.INTEGER),
        OperatorParserToken(OperatorTypes.MULTIPLICATION,
            NumberParserToken(2, NumberTypes.INTEGER),
            OperatorParserToken(OperatorTypes.EXPONENT,
                NumberParserToken(3, NumberTypes.INTEGER),
                NumberParserToken(4, NumberTypes.INTEGER)))))

# Source: "1 + 2 ^ 3 * 4"
# Tree:
#    +
#   / \
#  1  *
#    / \
#   ^  4
#  / \
# 2  3
def test_parse_operator_precedence_mixed_spike():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.PLUS),
        LexerToken(LexerTokenTypes.CARET),
        LexerToken(LexerTokenTypes.ASTRISK)
    ])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(1, NumberTypes.INTEGER) },
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(2, NumberTypes.INTEGER) },
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(3, NumberTypes.INTEGER) },
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(4, NumberTypes.INTEGER) }
    ])
    
    token = parse_expression(lex, parse)
    
    assert token == ExpressionParserToken(OperatorParserToken(OperatorTypes.ADDITION,
        NumberParserToken(1, NumberTypes.INTEGER),
        OperatorParserToken(OperatorTypes.MULTIPLICATION,
            OperatorParserToken(OperatorTypes.EXPONENT,
                NumberParserToken(2, NumberTypes.INTEGER),
                NumberParserToken(3, NumberTypes.INTEGER)),
            NumberParserToken(4, NumberTypes.INTEGER))))
            
# Source: "1 ^ 2 + 3 * 4"
# Tree:
#      +
#     / \
#    /   \
#   ^     *
#  / \   / \
# 1  2  3  4
def test_parse_operator_precedence_mixed_dip():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.CARET),
        LexerToken(LexerTokenTypes.PLUS),
        LexerToken(LexerTokenTypes.ASTRISK)
    ])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(1, NumberTypes.INTEGER) },
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(2, NumberTypes.INTEGER) },
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(3, NumberTypes.INTEGER) },
        { "type": ParserTokenTypes.CONSTANT, "value": NumberParserToken(4, NumberTypes.INTEGER) }
    ])
    
    token = parse_expression(lex, parse)
    
    assert token == ExpressionParserToken(OperatorParserToken(OperatorTypes.ADDITION,
        OperatorParserToken(OperatorTypes.EXPONENT,
            NumberParserToken(1, NumberTypes.INTEGER),
            NumberParserToken(2, NumberTypes.INTEGER)),
        OperatorParserToken(OperatorTypes.MULTIPLICATION,
            NumberParserToken(3, NumberTypes.INTEGER),
            NumberParserToken(4, NumberTypes.INTEGER))))

