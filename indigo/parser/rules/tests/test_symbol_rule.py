import pytest

from fakes import build_fake_lex
from indigo.lexer.lexer import LexerToken, LexerTokenTypes
from indigo.parser.models import *
from indigo.parser.rules.symbol_rule import parse_symbol

def test_parse_symbol():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.WORD, "username")
    ])
    
    token = parse_symbol(lex, None)        
    assert type(token) is SymbolParserToken
    assert "username" == token.value
    
def test_parse_reserved_word_lowercase():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.WORD, "let")
    ])
    
    token = parse_symbol(lex, None)
    assert token is None

def test_parse_reserved_word_uppercase():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.WORD, "LET")
    ])
    
    token = parse_symbol(lex, None)
    assert token is None
    
def test_parse_starts_with_reserved_word():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.WORD, "lettuce")
    ])
    
    token = parse_symbol(lex, None)        
    assert type(token) is SymbolParserToken
    assert "lettuce" == token.value

