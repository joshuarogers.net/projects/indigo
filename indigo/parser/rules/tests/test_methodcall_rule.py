import pytest

from fakes import build_fake_lex, build_fake_parse
from indigo.lexer.lexer import LexerToken, LexerTokenTypes
from indigo.parser.parser_token_types import ParserTokenTypes
from indigo.parser.models import *
from indigo.parser.rules.methodcall_rule import parse_methodcall

def test_parse_methodcall_parameterless():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.OPEN_PAREN),
        LexerToken(LexerTokenTypes.CLOSED_PAREN)
    ])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.SYMBOL, "value": SymbolParserToken("get_user") }
    ])
    token = parse_methodcall(lex, parse)
    
    assert type(token) is MethodCallParserToken
    assert "get_user" == token.method
    assert [] == token.arguments
    
def test_parse_methodcall_constant_argument():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.OPEN_PAREN),
        LexerToken(LexerTokenTypes.CLOSED_PAREN)
    ])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.SYMBOL, "value": SymbolParserToken("get_user") },
        { "type": ParserTokenTypes.EXPRESSION, "value": ExpressionParserToken(StringParserToken("localhost")) }
    ])
    token = parse_methodcall(lex, parse)
    
    assert type(token) is MethodCallParserToken
    assert "get_user" == token.method
    assert [ ExpressionParserToken(StringParserToken("localhost")) ] == token.arguments
    
def test_parse_methodcall_symbol_argument():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.OPEN_PAREN),
        LexerToken(LexerTokenTypes.CLOSED_PAREN)
    ])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.SYMBOL, "value": SymbolParserToken("get_user") },
        { "type": ParserTokenTypes.EXPRESSION, "value": ExpressionParserToken(SymbolParserToken("current_user")) }
    ])
    token = parse_methodcall(lex, parse)
    
    assert type(token) is MethodCallParserToken
    assert "get_user" == token.method
    assert [ ExpressionParserToken(SymbolParserToken("current_user")) ] == token.arguments
    
def test_parse_methodcall_nested_methodcall_argument():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.OPEN_PAREN),
        LexerToken(LexerTokenTypes.CLOSED_PAREN)
    ])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.SYMBOL, "value": SymbolParserToken("get_user") },
        { "type": ParserTokenTypes.EXPRESSION, "value": ExpressionParserToken(MethodCallParserToken("get_current_user", [])) }
    ])
    token = parse_methodcall(lex, parse)
    
    assert type(token) is MethodCallParserToken
    assert "get_user" == token.method
    assert [ ExpressionParserToken(MethodCallParserToken("get_current_user", [])) ] == token.arguments
    
def test_parse_methodcall_multiple_arguments():
    lex = build_fake_lex([
        LexerToken(LexerTokenTypes.OPEN_PAREN),
        LexerToken(LexerTokenTypes.COMMA),
        LexerToken(LexerTokenTypes.COMMA),
        LexerToken(LexerTokenTypes.CLOSED_PAREN)
    ])
    parse = build_fake_parse([
        { "type": ParserTokenTypes.SYMBOL, "value": SymbolParserToken("get_user") },
        { "type": ParserTokenTypes.EXPRESSION, "value": ExpressionParserToken(StringParserToken("localhost")) },
        { "type": ParserTokenTypes.EXPRESSION, "value": ExpressionParserToken(StringParserToken("jdoe")) },
        { "type": ParserTokenTypes.EXPRESSION, "value": ExpressionParserToken(NumberParserToken(0, NumberTypes.INTEGER)) }
    ])
    token = parse_methodcall(lex, parse)
    
    assert type(token) is MethodCallParserToken
    assert "get_user" == token.method
    assert [
        ExpressionParserToken(StringParserToken("localhost")),
        ExpressionParserToken(StringParserToken("jdoe")),
        ExpressionParserToken(NumberParserToken(0, NumberTypes.INTEGER))
    ] == token.arguments
    
