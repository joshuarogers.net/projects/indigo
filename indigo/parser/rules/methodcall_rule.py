from indigo.lexer.lexer import LexerTokenTypes
from indigo.parser.parser_token_types import ParserTokenTypes
from indigo.parser.models import *


def _parse_methodcall_parameters(lex, parse):
    arguments = []
    
    while True:
        argument = parse(ParserTokenTypes.EXPRESSION)
        if (argument is not None):
            arguments.append(argument)
            
        if (lex(LexerTokenTypes.COMMA) is None):
            break
    
    return arguments


def parse_methodcall(lex, parse):
    name = parse(ParserTokenTypes.SYMBOL)

    open_paren = lex(LexerTokenTypes.OPEN_PAREN)
    if open_paren is None:
        return None
    
    arguments = _parse_methodcall_parameters(lex, parse)

    closed_paren = lex(LexerTokenTypes.CLOSED_PAREN)
    if closed_paren is None:
        return None

    return MethodCallParserToken(name.value, arguments)

