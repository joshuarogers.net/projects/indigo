from .lexer.character_stream import CharacterStream
from .lexer.lexer import ForwardOnlyLexer, SeekableLexer, LexerTokenTypes
from .parser.parser import *
from indigo.parser.parser_factory import build

def build_lexer(text):
    character_stream = CharacterStream(text)
    forward_only_lexer = ForwardOnlyLexer(character_stream)
    seekable_lexer = SeekableLexer(forward_only_lexer)
    return seekable_lexer
    
def build_parser(text):
    lexer = build_lexer(text)
    parser = build(lexer)
    return parser

__all__ = [ "build_lexer", "build_parser", "ParserTokenTypes" ]
