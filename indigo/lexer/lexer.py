from enum import Enum


class LexerTokenTypes(Enum):
    WHITESPACE = 1
    OPEN_PAREN = 2
    CLOSED_PAREN = 3
    OPEN_CURLY = 4
    CLOSED_CURLY = 5
    COMMA = 6,
    MINUS = 7,
    PLUS = 8,
    LESSTHAN = 9,
    GREATERTHAN = 10
    EQUALS = 11
    PERIOD = 12
    NUMBER = 13
    STRING = 14
    WORD = 15
    EOL = 16
    COLON = 17
    ASTRISK = 18
    FORWARDSLASH = 19
    CARET = 20
    PIPE = 21
    MODULUS = 22
    BANG = 23
    INTERPOLATED_STRING = 24
    AMPERSAND = 25
    OCTOTHORPE = 80
    

class LexerToken:
    def __init__(self, type, value = None):
        self.type = type
        self.value = value    
        
    def __eq__(self, other):
        return type(self) == type(other) and self.type == other.type and self.value == other.value
        
    def __ne__(self, other):
        return not (self == other)


class LexerException(Exception):
    pass
            
        
class ForwardOnlyLexer():
    def __init__(self, character_stream):
        self._character_stream = character_stream
        self._simple_token_lookup = {
            ' ': LexerTokenTypes.WHITESPACE,
            '\t': LexerTokenTypes.WHITESPACE,
            '(': LexerTokenTypes.OPEN_PAREN,
            ')': LexerTokenTypes.CLOSED_PAREN,
            '{': LexerTokenTypes.OPEN_CURLY,
            '}': LexerTokenTypes.CLOSED_CURLY,
            ',': LexerTokenTypes.COMMA,
            '-': LexerTokenTypes.MINUS,
            '+': LexerTokenTypes.PLUS,
            '<': LexerTokenTypes.LESSTHAN,
            '>': LexerTokenTypes.GREATERTHAN,
            '=': LexerTokenTypes.EQUALS,
            '.': LexerTokenTypes.PERIOD,
            '\n': LexerTokenTypes.EOL,
            ':': LexerTokenTypes.COLON,
            '*': LexerTokenTypes.ASTRISK,
            '/': LexerTokenTypes.FORWARDSLASH,
            '^': LexerTokenTypes.CARET,
            '|': LexerTokenTypes.PIPE,
            '%': LexerTokenTypes.MODULUS,
            '!': LexerTokenTypes.BANG,
            '\n': LexerTokenTypes.EOL,
            '&': LexerTokenTypes.AMPERSAND,
            '#': LexerTokenTypes.OCTOTHORPE
        }
        
        self._lexer_rules = [
            (lambda x: x is None, lambda: None),
            (lambda x: x in self._simple_token_lookup, self._read_simple_token),
            (lambda x: x.isdigit(), self._read_digit),
            (lambda x: x.isalpha(), self._read_symbol),
            (lambda x: x == '"' or x == "'", self._read_string),
            (lambda x: x == '\r', self._read_newline)
        ]
    
    def _read_simple_token(self):
        current_char = self._character_stream.current
        token_type = self._simple_token_lookup[current_char]
        return LexerToken(token_type)

    def _read_digit(self):
        capture = self._character_stream.rewind_then_read_while(lambda y, x: y.isdigit())
        return LexerToken(LexerTokenTypes.NUMBER, capture)
        
    def _read_symbol(self):
        current_character = self._character_stream.current
        remaining_symbol_characters = self._character_stream.read_while(lambda y, x: y.isalpha() or y.isdigit() or y == "_")
        capture = current_character + remaining_symbol_characters
        
        return LexerToken(LexerTokenTypes.WORD, capture)
        
    def _read_string(self):
        current_char = self._character_stream.current
        capture = self._character_stream.read_while(lambda y, x: y != current_char or x == '\\')
        if self._character_stream.read() == None: # Throw away the end quote
            raise(LexerException("Unexpected end of string"))
        type = LexerTokenTypes.STRING if current_char == "'" else LexerTokenTypes.INTERPOLATED_STRING
        return LexerToken(type, capture)
        
    def _read_newline(self):
        next_char = self._character_stream.read()
        if next_char == '\n' :
            return LexerToken(LexerTokenTypes.EOL)
            
        line_number, column_number = self._character_stream.get_friendly_position()
        raise(LexerException("Invalid newline sequence encountered at {}:{}".format(line_number, column_number)))
        
    def get_next_token(self):
        current_char = None
        try:
            current_char = self._character_stream.read()
        except:
            raise(LexerException("Failed to read stream"))
    
        for condition, method in self._lexer_rules:
            if condition(current_char):
                return method()

        line_number, column_number = self._character_stream.get_friendly_position()
        raise(LexerException("Unexpected character '{}' at {}:{}".format(current_char, line_number, column_number)))


class SeekableLexer:
    def __init__(self, unbuffered_lexer):
        self._unbuffered_lexer = unbuffered_lexer
        self._current_pos = 0
        self._buffer = []
        
    def get_next_token(self):
        if len(self._buffer) <= self._current_pos:
            token = self._unbuffered_lexer.get_next_token()
            if token is None:
                return None
            self._buffer.append(token)
        
        token = self._buffer[self._current_pos]
        self._current_pos += 1
        return token

    def seek(self, pos):
        self._current_pos = max(0, min(pos, len(self._buffer)))
        
    def position(self):
        return self._current_pos

