import pytest
import unittest
from ..character_stream import CharacterStream
from ..lexer import ForwardOnlyLexer, LexerException, LexerTokenTypes, LexerToken, LexerToken, SeekableLexer

def read_lexer(lexer):
    tokens = []
    while True:
        token = lexer.get_next_token()
        if token == None:
            return tokens
        tokens.append(token)
        

def lex(text):
    character_stream = CharacterStream(text)
    forward_only_lexer = ForwardOnlyLexer(character_stream)
    seekable_lexer = SeekableLexer(forward_only_lexer)
    return read_lexer(seekable_lexer)


def test_none_type():
    with pytest.raises(LexerException):
        lex(None)

def test_space_is_whitespace():
    assert lex(" ") == [ LexerToken(LexerTokenTypes.WHITESPACE) ]
    
def test_tab_is_whitespace():
    assert lex("\t") == [ LexerToken(LexerTokenTypes.WHITESPACE) ]

def test_multiple_whitespace():
    assert lex("\t ") == [ LexerToken(LexerTokenTypes.WHITESPACE), LexerToken(LexerTokenTypes.WHITESPACE) ]

def test_open_paren():
    assert lex("(") == [ LexerToken(LexerTokenTypes.OPEN_PAREN) ]
    
def test_close_paren():
    assert lex(")") == [ LexerToken(LexerTokenTypes.CLOSED_PAREN) ]

def test_open_closed_paren():
    assert lex("()") == [ LexerToken(LexerTokenTypes.OPEN_PAREN), LexerToken(LexerTokenTypes.CLOSED_PAREN) ]

def test_open_space_closed_paren():
    assert lex("( )") == [ LexerToken(LexerTokenTypes.OPEN_PAREN), LexerToken(LexerTokenTypes.WHITESPACE), LexerToken(LexerTokenTypes.CLOSED_PAREN) ]
    
def test_open_curly():
    assert lex("{") == [ LexerToken(LexerTokenTypes.OPEN_CURLY) ]
            
def test_closed_curly():
    assert lex("}") == [ LexerToken(LexerTokenTypes.CLOSED_CURLY) ]
        
def test_open_closed_curly():
    assert lex("{}") == [ LexerToken(LexerTokenTypes.OPEN_CURLY), LexerToken(LexerTokenTypes.CLOSED_CURLY) ]
    
def test_comma():
    assert lex(",") == [ LexerToken(LexerTokenTypes.COMMA) ]

def test_minus():
    assert lex("-") == [ LexerToken(LexerTokenTypes.MINUS) ]

def test_plus():
    assert lex("+") == [ LexerToken(LexerTokenTypes.PLUS) ]
    
def test_minus():
    assert lex(".") == [ LexerToken(LexerTokenTypes.PERIOD) ]

def test_zero():
    assert lex("0") == [ LexerToken(LexerTokenTypes.NUMBER, "0") ]
    
def test_one_digit():
    assert lex("1") == [ LexerToken(LexerTokenTypes.NUMBER, "1") ]

def test_one_digit_negative():
    assert lex("-1") == [ LexerToken(LexerTokenTypes.MINUS), LexerToken(LexerTokenTypes.NUMBER, "1") ]

def test_decimal():
    assert lex("1.23") == [ LexerToken(LexerTokenTypes.NUMBER, "1"), LexerToken(LexerTokenTypes.PERIOD), LexerToken(LexerTokenTypes.NUMBER, "23") ]
    
def test_less_than():
    assert lex("<") == [ LexerToken(LexerTokenTypes.LESSTHAN) ]
    
def test_greater_than():
    assert lex(">") == [ LexerToken(LexerTokenTypes.GREATERTHAN) ]
    
def test_equals():
    assert lex("=") == [ LexerToken(LexerTokenTypes.EQUALS) ]
    
def test_assignment():
    assert lex("<-") == [ LexerToken(LexerTokenTypes.LESSTHAN), LexerToken(LexerTokenTypes.MINUS) ]
    
def test_doublequoted_string():
    assert lex('"Hello World"') == [ LexerToken(LexerTokenTypes.INTERPOLATED_STRING, "Hello World") ]

def test_doublequote_with_escaped_quote_string():
    assert lex('"Hello \\"World"') == [ LexerToken(LexerTokenTypes.INTERPOLATED_STRING, 'Hello \\"World') ]

def test_singlequoted_string():
    assert lex("'Hello World'") == [ LexerToken(LexerTokenTypes.STRING, "Hello World") ]

def test_single_with_escaped_quote_string():
    assert lex("'Hello World\\'s'") == [ LexerToken(LexerTokenTypes.STRING, "Hello World\\'s") ]
    
def test_singlequoted_string_with_doublequote():
    assert lex("'Hello \\\"World'") == [ LexerToken(LexerTokenTypes.STRING, "Hello \\\"World") ]

def test_doublequoted_string_with_singlequote():
    assert lex("\"Hello World's\"") == [ LexerToken(LexerTokenTypes.INTERPOLATED_STRING, "Hello World's") ]

def test_doublequoted_unmatched_string():
    with pytest.raises(LexerException):
        lex('"Hello World')
    
def test_single_unmatched_string():
    with pytest.raises(LexerException):
        lex("'Hello World")

def test_text_word():
    assert lex("hello") == [ LexerToken(LexerTokenTypes.WORD, "hello") ]

def test_text_number_word():
    assert lex("hello123") == [ LexerToken(LexerTokenTypes.WORD, "hello123") ]

def test_text_underscore_text_word():
    assert lex("hello_world") == [ LexerToken(LexerTokenTypes.WORD, "hello_world") ]

def test_text_number_underscore_text_word():
    assert lex("hello_123world") == [ LexerToken(LexerTokenTypes.WORD, "hello_123world") ]

def test_number_then_text_word():
    assert lex("321contact") == [ LexerToken(LexerTokenTypes.NUMBER, "321"), LexerToken(LexerTokenTypes.WORD, "contact") ]

def test_uppercase_text_word():
    assert lex("HELLO") == [ LexerToken(LexerTokenTypes.WORD, "HELLO") ]
    
def test_camelcase_text_word():
    assert lex("helloWorld") == [ LexerToken(LexerTokenTypes.WORD, "helloWorld") ]
            
def test_invalid_underscore_word():
    with pytest.raises(LexerException):
        lex("_helloworld")

def test_line_feed():
    assert lex("\n") == [ LexerToken(LexerTokenTypes.EOL) ]
    
def test_carriage_return():
    with pytest.raises(LexerException):
        lex("\rHello")

def test_line_feed_carriage_return():
    assert lex("\r\n") == [ LexerToken(LexerTokenTypes.EOL) ]
    
def test_colon():
    assert lex(":") == [ LexerToken(LexerTokenTypes.COLON) ]

def test_caret():
    assert lex("^") == [ LexerToken(LexerTokenTypes.CARET) ]
    
def test_astrisk():
    assert lex("*") == [ LexerToken(LexerTokenTypes.ASTRISK) ]
    
def test_forward_slash():
    assert lex("/") == [ LexerToken(LexerTokenTypes.FORWARDSLASH) ]
    
def test_pipe():
    assert lex("|") == [ LexerToken(LexerTokenTypes.PIPE) ]
    
def test_pipe_thru():
    assert lex("|>") == [ LexerToken(LexerTokenTypes.PIPE), LexerToken(LexerTokenTypes.GREATERTHAN) ]

def test_module():
    assert lex('%') == [ LexerToken(LexerTokenTypes.MODULUS) ]

def test_bang():
    assert lex('!') == [ LexerToken(LexerTokenTypes.BANG) ]

def test_ampersand():
    assert lex('&') == [ LexerToken(LexerTokenTypes.AMPERSAND) ]
    
def test_octothorpe():
    assert lex('#') == [ LexerToken(LexerTokenTypes.OCTOTHORPE) ]

def test_seekable_lexer_matches_base():
    character_stream = CharacterStream("abc 123")
    lexer = ForwardOnlyLexer(character_stream)
    seekable_lexer = SeekableLexer(lexer)        
    assert read_lexer(seekable_lexer) == lex("abc 123")

def test_seekable_lexer_rewinds():
    character_stream = CharacterStream("abc 123")
    lexer = ForwardOnlyLexer(character_stream)
    seekable_lexer = SeekableLexer(lexer)
    
    tokens1 = read_lexer(seekable_lexer)
    seekable_lexer.seek(0)
    tokens2 = read_lexer(seekable_lexer)
    assert tokens1 == tokens2

def test_seekable_lexer_seek_negative():
    character_stream = CharacterStream("abc 123")
    lexer = ForwardOnlyLexer(character_stream)
    seekable_lexer = SeekableLexer(lexer)
    
    seekable_lexer.seek(-1)
    assert seekable_lexer.position() == 0
    
def test_seekable_lexer_seek_beyond_furthest_consumed():
    character_stream = CharacterStream("abc 123")
    lexer = ForwardOnlyLexer(character_stream)
    seekable_lexer = SeekableLexer(lexer)
    seekable_lexer.seek(10)
    
    assert seekable_lexer.position() == 0
    seekable_lexer.get_next_token()
    seekable_lexer.seek(10)
    assert seekable_lexer.position() == 1

def test_seekable_lexer_read_beyond_end():
    character_stream = CharacterStream(" ")
    lexer = ForwardOnlyLexer(character_stream)
    seekable_lexer = SeekableLexer(lexer)
    seekable_lexer.get_next_token()
    
    assert seekable_lexer.position() == 1
    assert seekable_lexer.get_next_token() is None
    assert seekable_lexer.position() == 1

def test_lexer_read_beyond_end():
    character_stream = CharacterStream(" ")
    lexer = ForwardOnlyLexer(character_stream)
    lexer.get_next_token()
    assert lexer.get_next_token() is None
    

from unittest import defaultTestLoader, main, TextTestRunner

if __name__ == "__main__":
    suite = defaultTestLoader.discover(".")
    runner = TextTestRunner()
    runner.run(suite)
