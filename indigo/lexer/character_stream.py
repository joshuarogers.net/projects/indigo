class CharacterStream:
    def __init__(self, input_text):
        self.next_pos = 0    
        self.input_text = input_text
        self.current = None

    def has_next(self):
        return self.next_pos < len(self.input_text)
              
    def read(self):
        if not self.has_next():
            return None
        
        self.current = self.input_text[self.next_pos]
        self.next_pos += 1
        return self.current
    
    def rewind(self):
        self.next_pos -= 1
    
    def read_while(self, fn):
        start = self.next_pos
        current_char = None
        prev_char = None
    
        while self.has_next():
            prev_char = current_char
            current_char = self.read()
            if (fn(current_char, prev_char) != True):
                self.rewind()
                break
        return self.input_text[start:self.next_pos]
    
    def rewind_then_read_while(self, fn):
        self.rewind()
        return self.read_while(fn)
        
    def get_friendly_position(self):
        line_number = self.input_text.count('\n', 0, self.next_pos)
        last_newline = max(0, self.input_text.rfind('\n', 0, self.next_pos))
        column_number = self.next_pos - last_newline
        return (line_number, column_number)
