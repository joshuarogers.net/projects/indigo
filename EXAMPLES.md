let pointId <- interface(int)

let point <- interface({
    id: pointId,
    x: double,
    y: double
})

let points <- list({
    point({id: pointId(1),
            x: 1.0,
            y: 1.0}),
    point({id: pointId(2),
            x: 0f,
            y: 0f}),
})

let sqrt <- { n: set(int, double) } -> {
    n ^ 0.5
}

let distance <- { point1: point, point2: point } -> {
    let dx <- point1.x - point2.x
    let dy <- point1.y - point2.y
    sqrt((dx ^ 2) + (dy ^ 2))
}

for(points, { source_point } -> {
    let nearest_points <- map(points, { test_point } -> {
        { source: source_point,
          target: test_point,
          distance: distance(source_point, test_point) }
    }) |>
        sort({ point_measurement } -> { point_measurement.distance }) |>
        take(3)
    
    let nearest_point_ids <- map(nearest_points, {point} -> {point.id}) |>
        map(string) |>
        join(", ")

    print("${source_point.id}: ${nearest_point_ids}")
})

