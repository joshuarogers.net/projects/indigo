from enum import Enum
from indigo import *
from indigo.parser.models import *
from json import JSONEncoder
import json


class ParseTreeEncoder(JSONEncoder):
    def default(self, object):
        if isinstance(object, Enum):
            return object.name
        
        try:
            return object.__dict__
        except:
            return {}
            

program_text = """1 ^ 2 + 3 * 4 == 5"""

if __name__ == "__main__":
    parser = build_parser(program_text)
    tree = parser.parse(ParserTokenTypes.EXPRESSION)
    
    print(json.dumps(tree, indent=True, sort_keys=True, cls=ParseTreeEncoder))
